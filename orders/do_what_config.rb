# 在这里写入要怎样修改xml

# 每运行一次 就只能加key或者删除key

class DoWhatConfig
    
    # 是否加入key
    
    
    # 获取是否加入key
    def is_add_key
        @isAddKey = true;
    end
    
    # 以哈希值表示一个要加入的key
    # 删除的话 只要有keyName去匹配即可
    # 其他属性就不用写了
    # def xml_key
    #     {
    #         keyName: :temps
    #     }
    # end
    
    # 加key的话 在这边写
    def xml_key
        {
            parentName: 'FlashControlSettings', # insert就是要插入进去的父节点 没有的话 就是顶级
            keyName: 'aaa',
            type:"pt_list",
            node: {
                key: "aaa",
                value:"Punk",
                title:"Punk",
                desc:"Punk",
                valueSource:"Punk",
                valueLableSource:"Punk",
                defalutValue:"Punk",
                aTitle:"Punk"
                # childProperties
            }
            
        }
        
    end
    
    
    
    
    
end
