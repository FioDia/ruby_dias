# 批量给flashTemple.xml加上key或者删除key的程序
# 使用之前要先装ruby 然后在控制台写入ruby ...punk.rb完之后按回车即可
# (...是文件路径 就是放punk.rb的地方)
# 通过命令 文件

load 'orders/do_what_config.rb'
load 'injects/inject_file.rb'
load 'logics/parses.rb'


def start
    puts "...Hey Man Punk Now...#{Time.new}"
    
    init_config()
    
    init_inject()
    
    init_logic()
    
    init_file_saver()
    
end

# 读取配置属性
def init_config()
    @config = DoWhatConfig.new
end

# 文件准备
def init_inject()
    @file_arr = InjectFile.new.xml_files
end


# 增删节点
def init_logic()
    @parse = Parseser.new
    
    node = @parse.build_node(@config.xml_key)
    
    # puts node
    
    keyName = @config.xml_key[:keyName] || "name"
    
    parentName = @config.xml_key[:parentName] || "parentName"
    
    @file_arr.each do |path|
        # puts "path #{path} and nodes #{node}"
        
        if @config.is_add_key then
            @parse.insert_node path, node, keyName, parentName
        else
            @parse.delete_node_by_key keyName
        end
        
    end
end

# 保存文件
def init_file_saver()
end


# 从这里开始
start()