# xml解析工作都在这里完成
# 主要就是增加/删除子节点

require 'rexml/document'
include REXML

class Parseser
    
    def build_node(food)
        target_xml = Document.new("<property/>")
        target_xml.root.attributes["type"] = food[:type]
        food[:node].each do |k, v|
            e = target_xml.root.add_element k.to_s
            e.text = CData.new(v)
            puts e.text
        end
        
        target_xml.root.add_element 'childProperties'
        target_xml
    end

    # 给入文件路径
    def insert_node(xml_file_name, node, keyName, parentName)
        
        xml_sourece = load_xml_file(xml_file_name)
        xmldoc = Document.new(xml_sourece)
        
        # puts xmldoc
        return nil if xmldoc.to_s.match(keyName)
        
        
        # xmldoc.elements.first.elements["properties"].each do |e|
        #     puts e.elements["key"]
        # end
        
        if parentName.nil?
            # 直接在root上加节点
            
        else
            find_all_node(xmldoc, keyName, parentName, node, false) 
        end
        
        # 插入节点 成功！
        # puts  xmldoc
        
        # 直接执行文件保存
        
        return xmldoc
        
    end

    def delete_node_by_key(key)
        
    end
    
    
    # 私有方法
    private
    
    # 更具路径载入文件
        def load_xml_file(file_name)
            tf = File.new(file_name)
            tf
        end
    
    
    # 想法子遍历完所有xml节点
    # 递归递归
    def find_all_node(target_xml, keyName, parentName, lap_node, is_covering)
        #  艹 不想写了
        # 于是云9很配合的在8号挂了一天 2016年10月9日20:49:45
        
        # puts target_xml.elements.count
        
        target_xml.elements.each do |node|
            
            # puts "#{node.name} and #{parentName}"
            
            if node.text == parentName
                puts "node name? #{node.name} node text #{node.text}"
                # 看下面的子节点有没有对应的keyName 有就覆盖掉
                # 在childProperties里面找找有没keyName 有就覆盖 没有就直接插入
                # puts node.parent.elements['childProperties']
                
                
                if is_covering
                    puts "replace node #{node.name}"
                else
                    childs = node.parent.elements['childProperties']
                    
                    is_match = false
                    
                    if(!childs.nil?)
                        childs.each do |ele|
                            # 看能不能直接使用查找吧
                            if ele.elements['key'].text.match(keyName)
                                is_match = true
                                # 覆盖节点
                                puts ele
                            end
                        end
                    end
                    
                    unless is_match
                        # 直接加节点
                        childs.add lap_node
                        
                    end
                    
                end
                
                
            else
               find_all_node(node, keyName, parentName, lap_node, false) if node.has_elements?
            end
            # puts "node? #{node.name} " # 能找到名字了
            # puts "node has elements? #{node.has_elements?}"
        end
    end
    
end

    def find_node(target_xml) 
        # return "good bad ugly"
        return target_xml unless block_given?
        
        if yield(target_xml) 
            puts "get target 1 "
            return target_xml
        else
            
           target_xml.elements.each do |node|
            #  puts node.class  
             if yield(node)
                puts "get target 2" 
                # puts node.class
                # node
                return "good"
             else
                find_node(node) do |n|
                    yield(n)
                end
             end
           end
           
        end
        
    end


