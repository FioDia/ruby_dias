# 文件保存和备份xmldoc

# xml_simple = 

# <property type='pt_composite'>
#     <key><![CDATA[FlashControlSettings]]></key>
#     <value><![CDATA[]]></value><title>
#     <![CDATA[Flash Control Settings]]></title>
#     <desc><![CDATA[Set options for control settings in page-flipping eBooks]]></desc>
#     <valueSource><![CDATA[]]></valueSource>
#     <valueLableSource><![CDATA[]]></valueLableSource>
#     <defalutValue><![CDATA[]]></defalutValue>
#     <childProperties>
#         <property type='pt_form_security'>
#             <key><![CDATA[securitySetting]]></key>
#             <value><![CDATA[No Security]]></value>
#             <title><![CDATA[Security Settings]]></title>
#             <desc><![CDATA[Set security options to protect the flash page content.]]></desc>
#             <valueSource><![CDATA[Security]]></valueSource>
#             <valueLableSource><![CDATA[]]></valueLableSource>
#             <defalutValue><![CDATA[No Security]]></defalutValue>
#             <childProperties/>
#         </property>
#     <property type='pt_form_contactabout'>
#         <key><![CDATA[passwordTips]]></key>
#         <value><![CDATA[Please contact the <a href='mailto:author@sample.com'><u>author</u></a> to access the web]]></value>
#         <title><![CDATA[Password Tips]]></title>
#         <desc><![CDATA[You can customize the text for the password tips.
#             If you want to inform readers to get the password for the flipbook with password tips,
#             you can edit some message as the tips. Also you can lead readers to get the password tips by using the HTML link with the following format: 
#             <a href='http://www.flipbuilder.com/'>Flip Builder</a>]]>
#         </desc>
#         <valueSource><![CDATA[passwordTips]]></valueSource>
#         <valueLableSource><![CDATA[]]></valueLableSource>
#         <defalutValue><![CDATA[Please contact the <a href='mailto:author@sample.com'><u>author</u></a> to access the web]]></defalutValue>
#         <childProperties/>
#     </property>
#     <property type='pt_composite'>
#         <key><![CDATA[annotationConfig]]></key>
#         <value><![CDATA[]]></value>
#         <title><![CDATA[Link Settings]]></title>
#         <desc><![CDATA[Link settings...]]></desc>
#         <valueSource><![CDATA[]]></valueSource>
#         <valueLableSource><![CDATA[]]></valueLableSource>
#         <defalutValue><![CDATA[]]></defalutValue>
#         <childProperties>
#             <property type='pt_color'>
#                 <key><![CDATA[linkOverColor]]></key>
#                 <value><![CDATA[0x800080]]></value>
#                 <title><![CDATA[Link Color on Hover]]></title>
#                 <desc><![CDATA[The Link background color when the mouse is over it.]]></desc>
#                 <valueSource><![CDATA[]]></valueSource>
#                 <valueLableSource><![CDATA[]]></valueLableSource>
#                 <defalutValue><![CDATA[0x800080]]></defalutValue>
#                 <childProperties/>
#         </property>
#         <property type='pt_number'>
#             <key><![CDATA[linkOverColorAlpha]]></key>
#             <value><![CDATA[0.2]]></value>
#             <title><![CDATA[Link Opacity on Hover]]></title>
#             <desc><![CDATA[The alpha of the link.Value should be between 0 and 1]]></desc>
#             <valueSource><![CDATA[]]></valueSource>
#             <valueLableSource><![CDATA[]]></valueLableSource>
#             <defalutValue><![CDATA[0.2]]></defalutValue>
#             <childProperties/>
#         </property>
#         <property type='pt_list'>
#             <key><![CDATA[linkOpenedWindow]]></key>
#             <value><![CDATA[Blank]]></value>
#             <title><![CDATA[Open Links in...]]></title>
#             <desc><![CDATA[Set hyper links open window. Blank: open in a new window; Self: open in the same window of the flash.]]></desc>
#             <valueSource><![CDATA[Blank,Self]]></valueSource>
#             <valueLableSource><![CDATA[Blank,Self]]></valueLableSource>
#             <defalutValue><![CDATA[Blank]]></defalutValue>
#             <childProperties/>
#         </property>
#         <property aType='pt_bool' type='pt_list'>
#             <key><![CDATA[linkEnableWhenZoom]]></key>
#             <value><![CDATA[Enable]]></value>
#             <title><![CDATA[Enable Links in Zoom Mode]]></title>
#                 <aTitle><![CDATA[No]]></aTitle>
#                 <desc><![CDATA[Enabel/ Disable the link when Zoom In is enabled]]></desc>
#                 <valueSource><![CDATA[Disable,Enable]]></valueSource>
#                 <valueLableSource><![CDATA[Yes,No]]></valueLableSource>
#                 <defalutValue><![CDATA[Enable]]></defalutValue>
#                 <childProperties/>
#         </property>
#         </childProperties>
#         </property>
#         <property type='pt_text'>
#             <key><![CDATA[googleAnalyticsID]]></key>
#             <value><![CDATA[]]></value>
#             <title><![CDATA[Google Analytics ID]]></title>
#             <desc><![CDATA[In the HTML output format, you can add statistics by using Google Analytics. 
#                 You can get more infomation about Google Analytics ID from: http://www.google.com/analytics/]]></desc>
#             <valueSource><![CDATA[]]></valueSource>
#             <valueLableSource><![CDATA[]]></valueLableSource>
#             <defalutValue><![CDATA[]]></defalutValue>
#             <childProperties/>
#         </property>
#         </childProperties>
# </property>


